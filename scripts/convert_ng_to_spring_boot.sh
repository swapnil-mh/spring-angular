

# The Angular CLI can be used to generate new application scaffolding, as well as other things. It’s a useful starting point, but you could  # at this point grab any existing Angular app and put it in the same place. We want to work with the Angular app in the top level directory 
# to  keep all the tools and IDEs happy, but we also want make it look like a regular Maven build.
# Create the app with the CLI:

# $ ./ng new client # add --minimal here if you want to skip tests


cat $1/.gitignore >> .gitignore
rm -rf $1/node* $1/src/favicon.ico $1/.gitignore $1/.git
sed -i -e 's/node_/anode/' .gitignore
cp -rf $1/* .
cp $1/.??* .
rm -rvf $1
sed -i -e 's,dist/$1,target/$1/static,' angular.json


# We discarded the node modules that the CLI installed because we want the frontend plugin to do that work for us in an automated build. We 
# also edited the angular.json (a bit like a pom.xml for the Angular CLI app) to point the output from the Angular build to a location that 
# will be packaged in our JAR file.